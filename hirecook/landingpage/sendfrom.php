<?php

//var_dump($_POST);
//die();


/************************************************************************/
/* PHP CallBack v1.0                                                    */
/* ===========================                                          */
/*                                                                      */
/*   Written by Steve Dawson - http://www.stevedawson.com               */
/*   Freelance Web Developer - PHP, Perl and Javascript programming     */
/*   Website Design and Search Engine Submission Services               */
/* This program is free software. You can redistribute it and/or modify */
/************************************************************************/
## CONFIG - CHANGE THESE DETAILS TO SUIT
$YourEmail = "hello@hirecooks.com";       ## YOUR EMAIL ADDRESS
      ## YOUR EMAIL ADDRESS
//$YourEmail = "paulajeya@gmail.com";       ## YOUR EMAIL ADDRESS
$email = $_POST['email'];       ## YOUR EMAIL ADDRESS
##$WebsiteURL = "";  ## YOUR WEBSITE URL
$WebsiteURL = "hirecooks.com";  ## YOUR WEBSITE URL
$subject = "Business Enquiry";      ## THE EMAIL SUBJECT LINE
$ThankyouURL = "thankyou.html";      ##  THANKYOU PAGE 

## QUICK ERROR CHECK TO MAKE SURE THEY USED THE CALLBACK FORM
if(!isset($_POST['name'])) {
 header("location: $WebsiteURL");
 exit();
}

## CHECK TO GET THE SENDERS DETAILS
$date       = (date ("F j, Y"));
$time		= (date ("H:i:s"));
$IPnumber	= getenv("REMOTE_ADDR");
$Browser	= $_SERVER["HTTP_USER_AGENT"];
$ReferURL	= $_SERVER["HTTP_REFERER"];

$_POST['name'] = preg_replace("/[\n\r]+/", "", $_POST['name']);
$_POST['contact'] = preg_replace("/[\n\r]+/", "", $_POST['contact']);
$_POST['email'] = preg_replace("/[\n\r]+/", "", $_POST['email']);
$_POST['comments'] = preg_replace("/[\n\r]+/", "", $_POST['comments']);
$_POST['requirement']= preg_replace("/[\n\r]+/", "", $_POST['requirement']);
	
## THE EMAIL TEXT
$text = "
-----------------------------------------------------------------------------
hirecooks.com Landing Page form enquiry
-----------------------------------------------------------------------------

".$_POST['requirement']."


Name is: ".$_POST['name']."

Email is: ".$_POST['email']." 

Contact Number is: ".$_POST['contact']."

Message is: ".$_POST['comments']."

Kind Regards,
".$WebsiteURL."

this email was sent by hirecooks.com
---------------------------------------------------
Date:      ".$date." at ".$time."
IP Number: ".$IPnumber."
Browser:   ".$Browser."
Referrer   ".$ReferURL."
---------------------------------------------------
";
## STRIP OUT THE UNWANTED CHARACTERS INCASE ANY APPEAR
    $subject = stripslashes($subject);
    $text = stripslashes($text); 
	@mail("$YourEmail", $subject, $text, "From: <$email>"); ## Email Sent  
 
## The emails have both been sent, so we should now send the user to a confirmation page
 header("Location: $ThankyouURL"); ## Don't change this, change the $ThankyouURL at the top!
 exit;
## That's it !
 
?>
