/**
 * Created by viraj on 3/13/2019.
 */
angular.module('mainCtrl', ['authService'])
    .controller('mainController', function($scope,$http,$window, $rootScope, $location, Auth, AuthUser) {

        var vm = this;

        vm.loggedIn = Auth.isLoggedIn();
        $rootScope.$on('$routeChangeStart', function(){
            vm.loggedIn = Auth.isLoggedIn();
            vm.Username = AuthUser.getUser();

        });

        vm.doLogin = function() {

            console.log("i am in main dologin");
            vm.processing = true;
            vm.error = '';
            vm.Invalid = false;
            vm.InvalidUser = false;
            var userAgent = $window.navigator.userAgent;
            vm.loginData.userAgent = userAgent;
            Auth.login(vm.loginData.username, vm.loginData.password, vm.loginData.userAgent)
                .then(function (res) {

                    if (res == "Invalid password") {
                        vm.Invalid = true;
                        vm.processing = false;
                    }
                    if (res == "user dosent exist") {
                        vm.InvalidUser = true;
                        vm.processing = false;
                    }
                    if (res.Role == 'cooks') {
                        $location.path('/home');
                        vm.processing = false;
                    }
                    if (res.Role == 'admin') {
                        $location.path('/adminPage');
                        vm.processing = false;
                    }
                    if (res.Role == 'counsellor') {
                        $location.path('/counsellorPage');
                        vm.processing = false;
                    }
                    if (res.Role == 'superadmin') {
                        $location.path('/superAdminPage');
                        vm.processing = false;
                    }
                   /* else {
                        console.log("res err", res);
                        vm.error = res.data.message;
                    }*/
                });
        }

        vm.doLogout = function(){
            Auth.logout();
            $location.path('/home');
        }

        $(document).ready(function () {
            $('.bxslider_content').bxSlider({
                mode: 'fade',
                auto: true,
                autoControl: true,
                speed: 10
            });

            $('ul.nav li.dropdown-submenu').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(50);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(50);
            });
        });
    });
