/**
 * Created by viraj on 3/13/2019.
 */
angular.module('authService', [])

    .factory('Auth', function($http, $window, $location, $q, AuthToken,url){
        var authFactory = {};
        var nameIsNam;
        var loginUrl = url.urlBase+'/api/login';
        var logOutUrl = url.urlBase+'/api/logout';

        authFactory.login = function(username, password, userAgent){
            localStorage.getItem('token') ? nameIsNam = localStorage.getItem('token') : nameIsNam = null;
            if (nameIsNam == undefined || nameIsNam == null) {
                return $http.post(loginUrl, {
                    username: username,
                    password: password,
                    userAgent: userAgent
                }).then(function (res) {
                    if(res.data.message == "Invalid password"){
                        return "Invalid password";
                    }
                    if(res.data.message == "user dosent exist"){
                        return "user dosent exist";
                    }
                    if(res.data.message == "user dosent exist"){
                        return "user dosent exist";
                    }
                    if(res.data.message == "successfully login"){
                        AuthToken.setToken(res.data.token);
                        // $window.localStorage['token'] = res.data.token;
                        $window.localStorage['Role'] = res.data.data.Role;
                        $window.localStorage['userId'] = res.data.data._id;
                        $window.localStorage['username'] = res.data.data.username;
                        return res.data.data;
                    }
                    return res.data.data;
                },function error(res) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    console.log(res);
                    return res;
                });
            }else{
                return $http.post(loginUrl, {
                    username: username,
                    password: password
                }).then(function (res) {
                    AuthToken.setToken(res.data.token);
                    if(res.data.message == "Invalid password"){
                        return "Invalid password";
                    }
                    if(res.data.message == "user dosent exist"){
                        return "user dosent exist";
                    }
                    if(res.data.message == "successfully login"){
                        /*if( $window.localStorage['userId'] == res.data.data._id && $window.localStorage['Role'] == res.data.data.Role){
                            if(confirm('user already logged in do you want to continue')){
                                $window.localStorage['Role'] = res.data.data.Role;
                                $window.localStorage['userId'] = res.data.data._id;
                                $window.localStorage['username'] = res.data.data.username;
                                return res.data.data;

                            }else {
                                AuthToken.setToken(res.data.data.token);
                                $window.localStorage['Role'] = res.data.data.Role;
                                $window.localStorage['userId'] = res.data.data._id;
                                $window.localStorage['username'] = res.data.data.username;
                                return res.data.data;
                            }
                        }else {*/
                            AuthToken.setToken(res.data.token);
                            // $window.localStorage['token'] = res.data.token;
                            $window.localStorage['Role'] = res.data.data.Role;
                            $window.localStorage['userId'] = res.data.data._id;
                            $window.localStorage['username'] = res.data.data.username;
                            return res.data.data;
                        }
                    return res.data.data;
                });
            }
        }

        authFactory.logout = function(){
            $http.post(logOutUrl).then(function (res) {
                // console.log("res logout",res);
            });
            return AuthToken.setToken();
        }


        authFactory.isLoggedIn = function(){
            if(AuthToken.getToken()){
                return true;
            }
            else
                return false;
        }

        authFactory.getUsers = function(){
            if(AuthToken)
                return $http.get(url.urlBase+'/api/users');
            else
                return $q.reject({message:"user has no token"});
        }

        return authFactory;
    })
    .factory('AuthToken', function($window){
        var authTokenFactory = {};

        authTokenFactory.getToken = function(){
            var token = $window.localStorage.getItem('token');
            return token;
        }
        authTokenFactory.setToken = function(token){
            if(token)
                $window.localStorage.setItem('token', token);
            else
                $window.localStorage.removeItem('token');
            $window.localStorage.removeItem('username');
            $window.localStorage.removeItem('Role');
            $window.localStorage.removeItem('userId');
        }


        return authTokenFactory;
    })
    .factory('AuthInterceptor', function($q, $location, AuthToken){
        var interceptorFactory = {};

        interceptorFactory.request = function(config){
            var token = AuthToken.getToken();
            if(token){
                config.headers['x-access-token'] = token;
            }
            return config;
        }

        interceptorFactory.responseError = function(res){
            if(res.status == 403)
                $location.path('/home');
            return $q.reject(res);
        }
        return interceptorFactory;
    })
    .factory('AuthUser', function($window){
        var authUserFactory = {};
        authUserFactory.getUser = function(){
            var user = $window.localStorage['username'];
            return user;
        }
        authUserFactory.setUser = function(user){
            if(user)
                $window.localStorage['username'] = user;
            else
                $window.localStorage.removeItem('username');
            $window.localStorage.removeItem('Role');
            $window.localStorage.removeItem('userId');
        }

        return authUserFactory;
    })
    .factory('AuthenticationFactory', function ($window) {
        var auth = {
            isLogged: false,
            isAuthenticated: function () {
                if ($window.localStorage.token && $window.localStorage.userId) {
                    this.isLogged = true;
                } else {
                    this.isLogged = false;
                }
            }
        }

        return auth;
    })
    .factory('UserAuthenticationFactory',function ($window, $http) {
        return {
            isAuthenticatedForAccess: function (valueOfUrl) {
                return $http.post("/api/IsUserAuthenticatedForUrl", {'urlAddress':valueOfUrl,'Roles':localStorage.getItem('Role')},{
                    headers: {
                        'Content-Type': 'application/json',
                        'x-access-token': $window.localStorage['token']
                    }
                })
            }

        }
    })
    .factory('url', function() {
        return {

              urlBase : 'http://localhost:3000'
              //urlBase : 'http://localhost:21014'
           // urlBase : 'http://localhost:21011'
           	//urlBase : 'http://13.234.222.153:21012'
          // urlBase : 'http://13.233.219.225:22011'
             //urlBase : 'https://api.hirecooks.com'

        };
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push(function($q, $location) {
            return {
                responseError: function(rejection) {
                    if(rejection.status <= 0) {
                         //window.location = ;
                        $location.path('/connErr');

                        console.log("rejection",rejection);
                        return;
                    }
                    return $q.reject(rejection);
                }
            };
        });
    });


