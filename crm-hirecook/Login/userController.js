/**
 * Created by viraj on 25/04/2019.
 */
angular.module('userCtrl',[])
.controller('UserCreateController', function($window, User, $http){
	var uc = this;
    uc.signup = false;
    uc.errmsg=false;
    uc.psserr = false;
    // console.log("I am in usercontroller");
    uc.signupUser = function(){
        // console.log("userdata", uc.userData);

		if(uc.userData.password == uc.userData.password1) {
            uc.message = '';

            User.create(uc.userData).then(function (res) {

                uc.message = res.data.message;
                if (res.data.errmsg) {
                    uc.errmsg = true;
                }
                if (res.data.message == "user has been created") {
                    uc.signup = true;
                    // console.log(res);
                    var formData = new FormData();
                    formData.append('filename',uc.userData);
                    formData.append('userid',res.data.data._id);
                    $http.post('/api/uploadResume', formData).then(function (res) {
                        // console.log("filedata",res);

                    });
                    uc.userData = {};
                }

            });
        }else {
            uc.psserr = true;
		}
	}

});
