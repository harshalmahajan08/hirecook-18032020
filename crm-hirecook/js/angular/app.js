/**
 * Created by viraj on 3/13/2019.
 */

var myApp = angular.module('myApp', ['appRoutes','mainCtrl','authService','adminCtrl','counsellorCtrl','enquiryCtrl']);

$(document).ready(function () {
    $('.bxslider_content').bxSlider({
        mode: 'fade',
        auto: true,
        autoControl: true,
        speed: 10
    });

    $('ul.nav li.dropdown-submenu').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(50);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(50);
    });
});