/**
 * Created by viraj on 3/13/2019.
 */
var app = angular.module('appRoutes', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider,$locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.hashPrefix(''); // by default '!'
    // $locationProvider.html5Mode(true);
    $stateProvider.state('home', {
        url: "/",
        templateUrl: 'Login/login.html',
        controller: 'mainController'
    });
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'Login/login.html',
        controller: 'mainController'
    });
    $stateProvider.state('signup',{
        url: '/signup',
        templateUrl: 'Login/signup.html'
    });
    $stateProvider.state('adminPage',{
        url: '/adminPage',
        templateUrl:'Admin/adminPage.html',
        controller:'adminController',
        data: {
            required: true
        }
    });
    $stateProvider.state('counsellorPage',{
        url: '/counsellorPage',
        templateUrl:'Counsellor/counsellorPage.html',
        controller:'counsellorController',
        data: {
            required: true
        }
    });
    $stateProvider.state('thankyou',{
        url: '/thankyou',
        templateUrl:'thankyou.html'
    });
    $stateProvider.state('about-us',{
        url: '/about-us',
        templateUrl:'about-us.html'
    });

});
/*
.run(["$rootScope", "$state", "$location", "$cookies", "$sessionStorage", function ($rootScope, $state, $location, $cookies, $sessionStorage) {
    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        console.log("currentState: ", $state.current);
        console.log("toState is:", toState);

        if (toState.data.required && !$window.localStorage.userId) {
            alert("state not authenticated");
            e.preventDefault();
            $state.go('login');
        }
    });
}]);*/
