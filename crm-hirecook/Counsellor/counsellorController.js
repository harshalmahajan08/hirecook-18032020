/**
 * Created by viraj on 3/20/2019.
 */
var app = angular.module('counsellorCtrl', [])
    .controller('counsellorController', function ($scope, $http, $location, $window, Auth,url) {
// ...............................................Check If Accessing by url and role is not admin start................
        var Role = $window.localStorage['Role'];
        // console.log("User not defined",Role);
        if (Role == null || Role == undefined) {
            // console.log("User not defined",Role);
            $location.path('/login');

        }
        if (Role !== 'counsellor') {
            // console.log("User not defined",Role);
            Auth.logout();
            $location.path('/login');
        }
// ...............................................Check If Accessing by url and role is not counsellor end..................
        var userId = $window.localStorage.getItem("userId");
        var EnquiryData={};
        var EnquiryDataTemp ={};
        var tempData = {};
        var countOfSort = 0;
        var tempEnquiryList;
        var Consulting_For = [];
        var Require_Cooks = [];
        var Rent_Our_Kitchen  = [];
        var Consulting_For1= [];
        var Require_Cooks1 = [];
        var Rent_Our_Kitchen1 = [];
        $scope.UserDetails={};
        //get current logged in counselor
        EnquiryData.counsellorId = userId;
        tempData.UserId = userId;
        tempData.Role = "counsellor";
        $scope.resetPassData ={};
        $scope.commentData = [];
        $scope.commentsRemark = {};
        $scope.OrderFor = 'All';
        $scope.isAssignEnquiries = true;
        $scope.isViewMyProfile = false;

        GetAssignedEnquiry();
        lastLogin(tempData).then(function (res) {
            console.log("last login",res.data);
            $scope.userData= res.data[1];

        });

        getEnquiryByUserId().then(function (res) {
            $scope.EnquryFormOfCounsellor = res.data.data;

        });
        getUserDetailsByUserId(tempData).then(function (res) {
            $scope.UserDetails.User = res.data;
            console.log("$scope.UserDetails",$scope.UserDetails.User.username);
            $scope.Username =$scope.UserDetails.User.username;
        });

        $scope.sortDate = function(data) {
            var date = new Date(data);
            return date;
        };
        $scope.doLogout = function(){
            Auth.logout();
            $location.path('/login');
        }

        function GetAssignedEnquiry() {
            assignedEnquiry().then(function (res) {
                var AssignedEnquiry  = res.data;
                var AssignedEnquiryData = [];
                for(var resdata in AssignedEnquiry){
                    if(AssignedEnquiry[resdata].CounsellorId == EnquiryData.counsellorId ){
                        AssignedEnquiryData.push(AssignedEnquiry[resdata]);
                    }
                }
                $scope.getAssignApplications =  AssignedEnquiryData;
                $scope.getAssignApplicationsTemp =  AssignedEnquiryData;
                $scope.sortOrder($scope.getAssignApplications);
                sortOrderEnquiryType($scope.getAssignApplications);
                if ($scope.getAssignApplications.length == 0) {
                    $scope.noEnquires = true;
                }
                $scope.newAbandonRequest = getAbandonRequest($scope.getAssignApplications);
                showPaginationData($scope.getAssignApplications);
            });

        }

        $scope.sortOrder = function (data) {
            var sortData = data;
            tempEnquiryList = data;
            Consulting_For=[];
            Require_Cooks=[];
            Rent_Our_Kitchen=[];
            for (var sort1 in sortData) {
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Recipe Standardization' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Kitchen Setup' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Menu Planning' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Staff Training' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Operations' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Consulting For : Other'
                ) {
                    Consulting_For.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : One Day Event'||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Restaurant' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Café' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Fine Dining' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Commercial/Delivery Kitchen' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Food truck' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Home' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Other') {
                    Require_Cooks.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Food Trials'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Catering' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==' Rent Our Kitchen For : Get to Gathers' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Photography'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Events' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Other'
                ) {
                    Rent_Our_Kitchen.push(sortData[sort1]);
                }
            }

        }

        $scope.sortEnquiry = function (data) {
            countOfSort = data;
            $scope.getAssignApplications=[];
            if (countOfSort == 1) {
                $scope.getAssignApplications = Consulting_For;
                $scope.OrderFor = 'Consulting';
            }
            if (countOfSort == 2) {
                $scope.getAssignApplications = Require_Cooks;
                $scope.OrderFor = ' Require Cooks/Chefs';
            }
            if (countOfSort == 3) {
                $scope.getAssignApplications = Rent_Our_Kitchen;
                $scope.OrderFor = 'Rent Our Kitchen';
            }
            if (countOfSort == 4) {
                $scope.getAssignApplications = tempEnquiryList;
                $scope.OrderFor = 'All';
                countOfSort = 0;
            }
        }
        var prevId;
        $scope.SelectAssignApplication = function (data,id) {
            $('#id'+id).addClass("navliInboxactive");
            $('#id'+prevId).removeClass("navliInboxactive");
            prevId =id;
            $scope.NewEnquiryData = data;
            console.log("$scope.NewEnquiryData",$scope.NewEnquiryData);
        }


        /* ...................................................................................comment Start............
        */

        EnquiryDataTemp.creatorId =  EnquiryData.counsellorId;
        getCommentsByApplicantId(EnquiryDataTemp).then(function (res) {
            $scope.commentData = res.data;
            console.log($scope.commentData);
        });

        $scope.AddComments = function (data) {
            $scope.dataForSendComments = {};
            $scope.dataForSendComments.creatorId = EnquiryData.counsellorId;
            $scope.dataForSendComments.ApplicationId = data;
            $scope.dataForSendComments.commentsBy = "counsellor";
            $scope.dataForSendComments.content = $scope.addCommentData.content;
            $scope.dataForSendComments.commentsRemark = $scope.addCommentData.commentsRemark;
            $scope.isCommentAdded= true;

            addNewComment($scope.dataForSendComments).then(function (res) {
                $scope.commentResponse = res;
                $scope.addCommentData = {};
                $scope.commentData.push(res.data.data);
            });

        }
        $scope.Followupform = function (data) {
            $scope.FollowUp = true;
        }
        $scope.openForm = function (data) {
            document.getElementById("myForm").style.display = "block";
            var counseId = {};
            counseId.ApplicationId = data.enquiry._id;
            getCommentsByApplicationId(counseId).then(function (res) {
                console.log(res.data);
                $scope.commentData = res.data;

            });
        }

        $scope.closeForm = function () {
            document.getElementById("myForm").style.display = "none";
        }
        /*...................................................................................comment end............
        */


        $scope.ConvertForm = function (ApplicationData, status) {
            $scope.selectCoun1 = false;
            $scope.show = false;
            if(ApplicationData == undefined){
                $scope.SuccessMessgage = 'Please select enquiry';
                $('#ShowMessgae').modal('show');

            }else{
                if (status == 1) {
                    $('#exampleFormControlTextarea1').val('').change();
                    $scope.abondonBtn = false;
                    $scope.ConvertBtn = true;
                    $scope.LoadingCCAF = true;

                } if (status == 2) {
                    $scope.LoadingCCAF = true;
                    $('#exampleFormControlTextarea1').val('').change();
                    $scope.ConvertBtn = false;
                    $scope.abondonBtn = true;
                }
                if (status == 3) {
                    $('#exampleFormControlTextarea1').val('').change();
                    $scope.ConvertBtn1 = true;
                    $scope.ConvertBtn = false;
                    $scope.abondonBtn = false;
                }
                $('#exampleModalCenter').modal('show');
            }

        }
        $scope.ConvertEnq = function (ApplicationData, status, ckremarkData) {
            $scope.show = false;
            var ConvertFormData = {};
            var RemarkData = {};
            ConvertFormData.AssignDate = ApplicationData.AssignDate;
            ConvertFormData.counsellorId = ApplicationData.counsellorId;
            ConvertFormData.ApplicationNo = ApplicationData.enquiry.ApplicationNo;
            ConvertFormData.ApplicationId = ApplicationData.enquiry._id;

            if (status == 1) {
                if(ckremarkData != undefined || ckremarkData != null) {
                        RemarkData.creatorId = ConvertFormData.counsellorId;
                        RemarkData.ApplicationId = ConvertFormData.ApplicationId;

                        $scope.commentsRemark.RemarkBy = "counsellor";
                        RemarkData.commentsRemark = $scope.commentsRemark;
                        ConvertFormData.ApplicationStatus = "converted";
                        $scope.DataOfStay = "converted";

                        convertApplication(ConvertFormData).then(function (res) {
                            $scope.LoadingCCAF = false;
                            if (res.data.message == "Application converted successfully") {
                                $scope.converted = true;
                                $scope.isCommentAdded = false;
                                $scope.NewEnquiryData = {};
                                GetAssignedEnquiry();
                                $scope.SuccessMessgage = 'Enquiry has been converted successfully';
                                $scope.commentsRemark = {};
                                $scope.show = true;
                            }
                        });
                        addNewComment(RemarkData).then(function (res) {
                            $scope.commentsRemark = {};
                        });

                    makeApprovalFalse(RemarkData).then(function (res) {
                        console.log("res",res);
                      /*  if(res.data.message == "application false successfully"){
                            $scope.SuccessMessgage = 'Enquiry has been rejected';
                            $('#ShowMessgae').modal('show');
                        }*/
                    });
                } else {
                    $scope.pleaseSelectRemark = true;
                }

            }
            if (status == 2) {
                if(ckremarkData!= undefined || ckremarkData != null) {
                    /*if (ApplicationData.enquiry.isAbandonedReject == true) {
                        $scope.SuccessMessgage = 'Request is rejceted by Enquiry Admin';
                        $scope.show = true;
                        $scope.LoadingCCAF = false;
                    } else {*/
                        $scope.show = false;
                        RemarkData.creatorId = ConvertFormData.counsellorId;
                        RemarkData.ApplicationId = ConvertFormData.ApplicationId;
                        $scope.commentsRemark.RemarkBy = "counsellor";
                        RemarkData.commentsRemark = $scope.commentsRemark;
                        ConvertFormData.ApplicationStatus = "abandoned";
                        $scope.DataOfStay = "abandoned";

                        var apprveData = {};
                        apprveData.isAbandoned = true;
                        apprveData.ApplicationId = ConvertFormData.ApplicationId;

                        makeIsAbandonTrue(apprveData).then(function (res) {
                            $scope.LoadingCCAF = false;
                            if (res.data.success) {
                                $scope.converted = true;
                                $scope.isCommentAdded = false;
                                $scope.NewEnquiryData = {};
                                GetAssignedEnquiry();
                                $scope.SuccessMessgage = 'Request has been sent to the admin for approval';
                                $scope.commentsRemark = {};
                                $scope.show = true;
                            }
                        });
                            addNewComment(RemarkData).then(function (res) {
                            console.log("Remark", res.data);
                        });
                    // }
                }else {
                    $scope.pleaseSelectRemark = true;
                }
            }

        }

        $scope.UpdateCounsellorInfo = function(){
            $scope.LoadingUCI = true;
            $scope.CounsellorViewData = $scope.UserDetails.User;
            $scope.CounsellorViewData.UserId = $scope.CounsellorViewData._id;
            console.log("CounsellorViewData",$scope.CounsellorViewData);
            if(!isEmailValid($scope.CounsellorViewData.Email)){
                $scope.isInvalidEmail = true;
                $scope.LoadingUCI = false;
            }else {
                updateUserProfile($scope.CounsellorViewData).then(function (res) {
                    $scope.LoadingUCI = false;
                    if(res.data.message== "user successfully updated"){
                        $scope.isEditDiv=false;
                        $scope.SuccessMessgage = 'Counsellor has been updated';
                        $('#ShowMessgae').modal('show');
                    }
                });
            }
        }
        function isEmailValid(email)
        {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
            {
                return true;
            }
            return false;
        }

        $scope.setNewPassword = function () {
            $scope.LoadingCPC = true;
            if ($scope.resetPassData.newPassword !== $scope.resetPassData.againPassword) {
                $scope.NotMatch = true;
                $scope.LoadingCPC = false;
            } else {
                $scope.resetPassData.username = $scope.UserDetails.User.username;
                $scope.resetPassData.userId = $scope.UserDetails.User._id;

                changePassword($scope.resetPassData).then(function (res) {
                    $scope.LoadingCPC = false;
                    if (res.data.message == "Password Changed successfully") {
                        $scope.PassChanged = true;
                    }
                    if (res.data.message == "Invalid password") {
                        $scope.PassInvalid = true;
                    }if (res.data.message == "Please enter new password") {
                        $scope.PassAlredyInUse = true;
                    }
                });
            }
        }

        $scope.FuncProfile = function () {
            $("#ProfileNav").addClass("navliactive");
            $("#EnquiryStatusNav").removeClass("navliactive");
            $("#EnquiriesNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.isAssignEnquiries = false;
            $scope.isProfile = true;
            $scope.WorkStatus = false;
        }
        $("#EnquiriesNav").addClass("navliactive");
        $scope.FuncEnquires = function () {
            $("#EnquiriesNav").addClass("navliactive");
            $("#EnquiryStatusNav").removeClass("navliactive");
            $("#ProfileNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.isAssignEnquiries = true;
            $scope.isProfile = false;
            $scope.WorkStatus = false;
        }
        $scope.FuncWorkStatus = function () {
            $("#EnquiryStatusNav").addClass("navliactive");
            $("#EnquiriesNav").removeClass("navliactive");
            $("#ProfileNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.isAssignEnquiries = false;
            $scope.WorkStatus = true;
            $scope.isProfile = false;

        }


        $scope.ChangeTabInProfile = function (data) {
            if(data == 'MyProfile'){
                $scope.isViewMyProfile=false;
                $scope.ischangePassword=false;
            }
            if(data == 'MyPassword'){
                $scope.isViewMyProfile=true;
                $scope.ischangePassword=true;
            }

        }



        /*sorting for work status*/
        $scope.checkNewAbandonRequest = function(){
            if($scope.newAbandonRequest.length > 0){
                $scope.getAssignApplications = $scope.newAbandonRequest;
                showPaginationData($scope.getAssignApplications);
            }else {

            }
        }

        $scope.refreshSort = function(){
            document.getElementById('sortType').selectedIndex = 0;
            document.getElementById('sortStatus').selectedIndex = 0;
            GetAssignedEnquiry();
            $scope.getAssignApplications =  $scope.getAssignApplicationsTemp;
            showPaginationData($scope.getAssignApplications);
        }

        $scope.SortEnquiry = function(){
            var SortData = $scope.sortInWorkStatusEnquiryData;
            var DataForSort = $scope.getAssignApplications;
            var DataForSortTemp = $scope.getAssignApplicationsTemp;
            var getType,CounsData,getCounsellor,getStatus,StatusData;
            console.log("SortData",SortData);
            console.log("$scope.getAssignApplications",$scope.getAssignApplications);

            if(SortData == undefined || SortData == null){
                $scope.getAssignApplications = DataForSortTemp;
            }
            if(SortData.Type){
                getType = sortWorkEnquiry(SortData.Type);
                $scope.getAssignApplications = getType;
                showPaginationData($scope.getAssignApplications);
            }
            if(SortData.Status){
                StatusData = DataForSortTemp;
                getStatus = sortOrderWorkStatus(StatusData, SortData.Status);
                $scope.getAssignApplications = getStatus;
                showPaginationData($scope.getAssignApplications);
            }
            if(SortData.Type && SortData.Status){
                getType = sortWorkEnquiry(SortData.Type);
                getStatus = sortOrderWorkStatus(getType, SortData.Status);
                $scope.getAssignApplications = getStatus;
                showPaginationData($scope.getAssignApplications);
            }

        }


        $scope.sortInWorkStatusEnquiry = function(sortFor){
            console.log("sortFor",sortFor);
            var tempArray = $scope.getAssignApplicationsTemp;
            if(sortFor == 'Status'){
                sortOrderWorkStatus(tempArray ,$scope.sortInWorkStatusEnquiryData);
            }
            if(sortFor == 'Counsellor'){
                sortOrderWorkCounsellor(tempArray ,$scope.sortInWorkStatusEnquiryData);

            }if(sortFor == 'EnquiryType'){
                sortWorkEnquiry($scope.sortInWorkStatusEnquiryData);
            }

        }



        function sortOrderEnquiryType (data) {
            var sortData = data;
            tempEnquiryList = data;
            Consulting_For1= [];
            Require_Cooks1 = [];
            Rent_Our_Kitchen1 = [];

            for (var sort1 in sortData) {
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Recipe Standardization' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Kitchen Setup' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Menu Planning' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Staff Training' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Operations' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Consulting For : Other'
                ) {
                    Consulting_For1.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : One Day Event'||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Restaurant' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Café' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Fine Dining' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Commercial/Delivery Kitchen' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Food truck' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Home' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Other') {
                    Require_Cooks1.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Food Trials'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Catering' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==' Rent Our Kitchen For : Get to Gathers' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Photography'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Events' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Other'
                ) {
                    Rent_Our_Kitchen1.push(sortData[sort1]);
                }
            }

        }
        function sortWorkEnquiry (data) {
            countOfSort = data;
            if (countOfSort == 1) {
                return Consulting_For1;
            }
            if (countOfSort == 2) {
                return Require_Cooks1;
            }
            if (countOfSort == 3) {
                return Rent_Our_Kitchen1;
            }
            if (countOfSort == 4) {
                return tempEnquiryList;
                countOfSort = 0;
            }
        }
        function sortOrderWorkStatus(data, Status) {
            var sortData = data;
            var status = Status;
            var ForPending=[];
            var ForConverted=[];
            var ForAbandoned=[];
            for (var sort1 in sortData) {
                if (sortData[sort1].enquiry.ApplicationStatus =="pending") {
                    ForPending.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.ApplicationStatus =="converted") {
                    ForConverted.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.ApplicationStatus =="abandoned") {
                    ForAbandoned.push(sortData[sort1]);
                }
            }
            return AddStatus(status,sortData, ForPending,ForConverted,ForAbandoned);
        }
        function AddStatus(Status,data, ForPending1,ForConverted1,ForAbandoned1) {
            var ForPending=ForPending1;
            var ForConverted=ForConverted1;
            var ForAbandoned=ForAbandoned1;

            if(Status == 'pending'){
                return ForPending;
            }if(Status == 'converted'){
                return ForConverted;
            }if(Status == 'abandoned'){
                return ForAbandoned;
            }if(Status == 'All'){
                return data;
            }
        }
        function getAbandonRequest(data) {
            var abrData = [];
            for(var abr in data){
                if(data[abr].enquiry.isAbandoned == true){
                    abrData.push(data[abr]);
                }
            }
            return abrData;
        }

        /*sorting for work status*/

        /* pagination for work status/*/
        function showPaginationData(data){

            $scope.curPage = 0;
            $scope.pageSize = 10;
            $scope.datalists = [];
            $scope.datalists = data;
            $scope.numberOfPages = function() {
                return Math.ceil($scope.datalists.length / $scope.pageSize);
            };
            // console.log("number of pages",$scope.numberOfPages);
        }

        $scope.Reassigncounsellor = function (data) {
            $scope.isReassign = true;
            $scope.ReassignData = data;
            $scope.getApplicationRemark(data);
            $scope.selectCounsellorData = {};
        }

        $scope.getApplicationRemark = function (data) {
            var counseId = {};
            counseId.ApplicationId = data.enquiry._id;
            getCommentsByApplicationId(counseId).then(function (res) {
                $scope.ClosingRemark = res.data;
            });
        }

        $scope.Followupform = function (data) {
            $scope.FollowUp = true;
        }
        $scope.openForm1 = function (data) {
            document.getElementById("myForm1").style.display = "block";
            var counseId = {};
            counseId.ApplicationId = data.enquiry._id;
            getCommentsByApplicationId(counseId).then(function (res) {
                console.log(res.data);
                $scope.commentData = res.data;

            });
        }

        $scope.closeForm1 = function () {
            document.getElementById("myForm1").style.display = "none";
        }



        function lastLogin(data) {
            var getData = data;
            var lastloginUrl = url.urlBase+'/api/lastlogin';
          return $http.post(lastloginUrl, getData,{
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function getUserDetailsByUserId(data) {
            var getData = data;
            var getUserByIdUrl = url.urlBase + '/api/getUserById';
            return $http.post(getUserByIdUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function getEnquiryByUserId(){
            var getEnquiryFormUrl = url.urlBase+'/api/getUserEnquiry';
            return $http.get(getEnquiryFormUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function assignedEnquiry() {
            var getAssignedEnquiryFormUrl = url.urlBase+'/api/getAssignedEnquiryForm';
            return $http.get(getAssignedEnquiryFormUrl,{
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }

        function addNewComment(data) {
            var CommentData = data;
            var addCommentsUrl = url.urlBase + '/api/addComments';
            return $http.post(addCommentsUrl, CommentData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }

        function getCommentsByApplicantId(data) {
            var getData = data;
            var getCommentsByApplictionIdUrl = url.urlBase + '/api/getCommentsByApplicationId';
            return $http.post(getCommentsByApplictionIdUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function getCommentsByApplicationId(data) {
            var getData = data;
            var getCommentsByCommentsApplicationIdUrl = url.urlBase + '/api/getCommentsByCommentsApplicationId';
            return $http.post(getCommentsByCommentsApplicationIdUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function changePassword(data) {
            var getData = data;
            var setNewPasswordUrl = url.urlBase + '/api/setNewPassword';
            return $http.post(setNewPasswordUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            })
        }
        function updateUserProfile(data) {
            var getData = data;
            var updateUserProfileUrl = url.urlBase+'/api/updateUserProfile';
            return $http.post(updateUserProfileUrl,getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function makeApprovalFalse(data) {
            var getData =data;
            var falseApprovalUrl = url.urlBase + '/api/falseApproval';
           return $http.post(falseApprovalUrl,getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function convertApplication(data) {
            var getData = data;
            var convertApplicationUrl = url.urlBase + '/api/convertApplication';
            return $http.post(convertApplicationUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }

        function makeIsAbandonFalse(data) {
            var canelData = data;
            var cancelAbandonedUrl = url.urlBase +'/api/cancelAbandoned';
            return $http.post(cancelAbandonedUrl,canelData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function makeIsAbandonTrue(data) {
            var getData = data;
            var addForApprovedEnquiryUrl = url.urlBase + '/api/makeAbandoned';
            return $http.post(addForApprovedEnquiryUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }






        $(document).ready(function() {
            $('.bxslider_content').bxSlider({
                mode: 'fade',
                auto: true,
                autoControl: true,
                speed: 10
            });

            $('ul.nav li.dropdown-submenu').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(50);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(50);
            });
        });

    }).filter('pagination', function() {
        return function(input, start)
        {
            if(input == undefined){
                var input;
                console.log("input null");
            }
            start = +start;
            return input.slice(start);
        };
    });