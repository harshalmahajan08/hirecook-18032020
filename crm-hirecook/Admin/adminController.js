/**
 * Created by viraj on 04/25/2019.
 */
angular.module('adminCtrl', [])
    .controller('adminController', function ($scope, $http, $window, $location, Auth, url) {

        console.log("I am in admin controller");
// ...............................................Check If Accessing by url and role is not admin start................
        var Role = $window.localStorage['Role'];
        if (Role == null || Role == undefined) {
            Auth.logout();
            $location.path('/home');
        }
        if (Role !== 'admin') {
            Auth.logout();
            $location.path('/home');
        }
// ...............................................Check If Accessing by url and role is not admin end..................
        $scope.assigned = false;
        $scope.AssignEnquiries = true;
        $scope.WorkStatus = false;
        $scope.EnquiryForms = false;
        $scope.NewEnquiryMultipleData = [];
        $scope.resetPassData = {};
        $scope.commentsRemark = {};
        $scope.selectCounsellorData = {};
        $scope.AllEnquiryOfCounsellor = [];
        $scope.sortInWorkStatusEnquiryData={};
        $scope.counsellorId = {};
        var Consulting_For = [];
        var Require_Cooks = [];
        var Rent_Our_Kitchen = [];
        var Consulting_For1 = [];
        var Require_Cooks1 = [];
        var Rent_Our_Kitchen1 = [];
        var tempData = {};
        var getCounsellors = {};
        var countOfSort = 0;
        var tempEnquiryList;
        $scope.OrderFor = 'All';
        tempData.Role = "counsellor";
        getCounsellors.Role = "counsellor";
        tempData.UserId = $window.localStorage['userId'];
        getCounsellors.UserId = $window.localStorage['userId'];
        var AdminUserId = $window.localStorage['userId'];
        $scope.UserDetails = {};
        var prevId;

        GetEnquiryListNotAssigned();
        GetAssignedEnquiry();
        GetAllEnquiry();
        GetCounsellor(getCounsellors);


        var getUserByIdUrl = url.urlBase + '/api/getUserById';
        $http.post(getUserByIdUrl, getCounsellors, {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': $window.localStorage['token']
            }
        }).then(function (res) {
            $scope.UserDetails.User = res.data;
            lastLogin(tempData);
        });

        function GetEnquiryListNotAssigned() {
            var getEnquiryListNotAssignedUrl = url.urlBase + '/api/getEnquiryListNotAssigned';
             $http.get(getEnquiryListNotAssignedUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            }).then(function (res) {
                $scope.EnquiryList = res.data;
                $scope.EnquiryListTemp = $scope.EnquiryList;
                $scope.sortOrder($scope.EnquiryList);

            });
        }

        function GetCounsellor(tempData) {
            var getUserByRoleUrl = url.urlBase + '/api/getUserByRole';

            $http.post(getUserByRoleUrl, tempData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            }).then(function (res) {
                $scope.CounsellorsList = res.data;
                // console.log("consellordata",$scope.CounsellorsList);
            });
        }

        function GetAllEnquiry() {
            var getUserEnquiryUrl = url.urlBase + '/api/getUserEnquiry';
            $http.get(getUserEnquiryUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            }).then(function (res) {
                var statusList = [];
                $scope.CountList = {};
                for (var list in res.data) {
                    statusList.push(res.data[list].ApplicationStatus);
                }
                statusList.forEach(function (i) {
                    $scope.CountList[i] = ($scope.CountList[i] || 0) + 1;
                });
            });
        }

        function GetAssignedEnquiry() {
            var getAssignedEnquiryFormUrl = url.urlBase + '/api/getAssignedEnquiryForm';
            $http.get(getAssignedEnquiryFormUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            }).then(function (response) {
                $scope.getAssignApplications = response.data;
                $scope.getAssignApplicationsTemp = $scope.getAssignApplications;
                if ($scope.getAssignApplications.length == 0) {
                    $scope.noEnquires = true;
                }
                sortOrderEnquiryType ($scope.getAssignApplications);
               $scope.newAbandonRequest = getAbandonRequest($scope.getAssignApplications);
                showPaginationData($scope.getAssignApplications);
            });
        }

        function lastLogin(tempData) {
            var lastloginUrl = url.urlBase + '/api/lastlogin';
            $http.post(lastloginUrl, tempData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            }).then(function (res) {
                $scope.UserDetails.login = res.data[1];
            });
        }

        $scope.sortDate = function (data) {
            var date = new Date(data);
            return date;
        };


        $scope.sortOrder = function (data) {
            var sortData = data;
            Consulting_For = [];
            Require_Cooks = [];
            Rent_Our_Kitchen = [];
            for (var sort1 in sortData) {
                if (sortData[sort1].RequireCookChefFor == 'Consulting For : Recipe Standardization' ||
                    sortData[sort1].RequireCookChefFor == 'Consulting For : Kitchen Setup' ||
                    sortData[sort1].RequireCookChefFor == 'Consulting For : Menu Planning' ||
                    sortData[sort1].RequireCookChefFor == 'Consulting For : Staff Training' ||
                    sortData[sort1].RequireCookChefFor == 'Consulting For : Operations' ||
                    sortData[sort1].RequireCookChefFor == 'Consulting For : Other'
                ) {
                    Consulting_For.push(sortData[sort1]);
                }
                if (sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : One Day Event' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Restaurant' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Café' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Fine Dining' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Commercial/Delivery Kitchen' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Food truck' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Home' ||
                    sortData[sort1].RequireCookChefFor == 'Require Cooks/Chefs For : Other') {
                    Require_Cooks.push(sortData[sort1]);
                }
                if (sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Food Trials' ||
                    sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Catering' ||
                    sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Get to Gathers' ||
                    sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Photography' ||
                    sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Events' ||
                    sortData[sort1].RequireCookChefFor == 'Rent Our Kitchen For : Other'
                ) {
                    Rent_Our_Kitchen.push(sortData[sort1]);
                }
            }

        }
        $scope.sortEnquiry = function (data) {
            countOfSort = data;
            if (countOfSort == 1) {
                $scope.EnquiryList = Consulting_For;
                $scope.OrderFor = 'Consulting';
                $scope.NewEnquiryMultipleData = [];
                $scope.isMultiple = false;
                UnSelectAll();
            }
            if (countOfSort == 2) {
                $scope.EnquiryList = Require_Cooks;
                $scope.OrderFor = ' Require Cooks/Chefs';
                $scope.NewEnquiryMultipleData = [];
                $scope.isMultiple = false;
                UnSelectAll();
            }
            if (countOfSort == 3) {
                $scope.EnquiryList = Rent_Our_Kitchen;
                $scope.OrderFor = 'Rent Our Kitchen';
                $scope.NewEnquiryMultipleData = [];
                $scope.isMultiple = false;
                UnSelectAll();
            }
            if (countOfSort == 4) {
                $scope.EnquiryList = $scope.EnquiryListTemp;
                $scope.OrderFor = 'All';
                countOfSort = 0;
                $scope.NewEnquiryMultipleData = [];
                $scope.isMultiple = false;
                UnSelectAll();
            }
        }

        $scope.assignApplicationsToCounsellor = function (enquiry) {

            if (enquiry == undefined) {
                $scope.SuccessMessgage = 'please select Enquiry';
                $('#ShowMessgae').modal('show');
            } else if ($scope.selectCounsellorData == undefined || $scope.selectCounsellorData.CounsellorId == undefined) {
                $scope.SuccessMessgage = 'please select Counsellor';
                $('#ShowMessgae').modal('show');
            } else {
                tempData = {};
                $scope.assigned = false;
                tempData.AssignTo = $scope.selectCounsellorData.CounsellorId;
                tempData.AssignFrom = $window.localStorage['userId'];
                tempData.ApplicationId = enquiry._id;
                tempData.ApplicationStatus = enquiry.ApplicationStatus;

                $scope.LoadingAAC = true;
                assignSingleApplication(tempData).then(function (res) {
                    $scope.LoadingAAC = false;
                    if (res.data.message == "application assigned successfully") {
                        $scope.assigned = true;
                        $scope.NewEnquiryData = {};
                        $scope.NewEnquiryMultipleData = [];
                        $scope.selectCounsellorData = {};
                        $scope.isMultiple = false;
                        UnSelectAll();
                        GetEnquiryListNotAssigned();
                        GetAssignedEnquiry();
                        $scope.SuccessMessgage = 'Enquiry has been assigned successfully';
                        $('#ShowMessgae').modal('show');
                    }
                });
            }
        }

        $scope.SelectNewEnquiry = function (data, id) {
            $('#id' + id).addClass("navliInboxactive");
            $('#id' + prevId).removeClass("navliInboxactive");
            prevId = id;
            $scope.isMultiple = false;
            $scope.NewEnquiryData = data;
            $scope.NewEnquiryMultipleData =[];
            UnSelectAll();
        }
        $scope.SelectNewEnquiryMultiple = function (newEnq) {
            $scope.isMultiple = true;
            if ($scope.NewEnquiryMultipleData.length == 0) {
                $scope.NewEnquiryMultipleData.push(newEnq);
            } else {
                var idx = isElemntDups($scope.NewEnquiryMultipleData, newEnq);
                if (idx) {
                    $scope.NewEnquiryMultipleData.splice(idx, 1);
                    if ($scope.NewEnquiryMultipleData.length == 0) {
                        $scope.isMultiple = false;
                    }
                } else {
                    $scope.NewEnquiryMultipleData.push(newEnq);
                }
            }
            function isElemntDups(arry, arry1) {
                for (var idx in arry) {
                    if (arry[idx]._id === arry1._id) {
                        return idx;
                    }
                }
            }
        }

        $scope.assignMultiApplicationsToCounsellor = function () {
          var multiEnquiry = [];
            multiEnquiry = $scope.NewEnquiryMultipleData;

            if (multiEnquiry == undefined) {
                $scope.SuccessMessgage = 'please select Enquiry';
                $('#ShowMessgae').modal('show');

            } else if ($scope.selectCounsellorData == undefined || $scope.selectCounsellorData.CounsellorId == undefined) {
                $scope.SuccessMessgage = 'please select Counsellor';
                $('#ShowMessgae').modal('show');
            } else {
                var tempData = {};
                var senMulti = [];
                var user = $window.localStorage['userId'];
                $scope.assigned = false;
                for (var mltEnq in multiEnquiry) {
                    tempData.AssignTo = $scope.selectCounsellorData.CounsellorId;
                    tempData.AssignFrom = user;
                    tempData.ApplicationId = multiEnquiry[mltEnq]._id;
                    tempData.ApplicationStatus = multiEnquiry[mltEnq].ApplicationStatus;
                    senMulti.push(tempData);
                    tempData = {};
                }
                var assignData = {};
                assignData.AddData = senMulti;
                assignData.AssignTo = $scope.selectCounsellorData.CounsellorId;
                $scope.LoadingAAC = true;
                assignMutliApplication(assignData).then(function (res) {
                    $scope.LoadingAAC = false;
                    if (res.data.message == "application assigned successfully") {
                        UnSelectAll();
                        $scope.assigned = true;
                        $scope.selectCounsellorData = {};
                        multiEnquiry=null;
                        GetEnquiryListNotAssigned();
                        GetAssignedEnquiry();
                        $scope.NewEnquiryData = {};
                        $scope.isMultiple = false;
                        $scope.NewEnquiryMultipleData = [];
                        $scope.SuccessMessgage = 'Enquiry has been assigned successfully';
                        $('#ShowMessgae').modal('show');
                    }
                });
            }

        }
        function UnSelectAll() {
            var items = document.getElementsByName('myCheck');
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox')
                    items[i].checked = false;
            }
        }

        $scope.updateAssignApplicationsToCounsellor = function (enquiry) {

            $scope.selectCoun = false;
            if ($scope.selectCounsellorData == undefined || $scope.selectCounsellorData.CounsellorId == undefined) {
                $scope.SuccessMessgage = 'Please select counsellor';
                $('#ShowMessgae').modal('show');

            } else if (enquiry.CounsellorId == $scope.selectCounsellorData.CounsellorId) {
                $scope.SuccessMessgage = 'Counsellor already assigned,';
                $('#ShowMessgae').modal('show');
            } else {
                $scope.calledByReassign = false;
                tempData = {};
                tempData.assignId = enquiry._id;
                tempData.AssignTo = $scope.selectCounsellorData.CounsellorId;
                tempData.AssignFrom = $window.localStorage['userId'];
                tempData.ApplicationId = enquiry.enquiry._id;

                if(enquiry.enquiry.ApplicationStatus == 'abandoned'){
                    tempData.ApplicationStatus = 'pending';
                }else {
                    tempData.ApplicationStatus = enquiry.enquiry.ApplicationStatus;
                }

                $scope.LoadingWUFAD = true;
                reAssignCounsellor(tempData).then(function (res) {
                  $scope.LoadingWUFAD = false;
                    if (res.data.message == "application assigned updated") {
                        if($scope.ReassignData.enquiry.isAbandoned == true){
                            $scope.calledByReassign = true;
                            $scope.rejectApproval();
                        }
                        $scope.ReassignData.CounsellorName = $scope.selectCounsellorData.CounsellorName;
                        $scope.updateAssigned = true;
                        GetAssignedEnquiry();
                        $scope.selectCounsellorData = {};
                        $scope.SuccessMessgage = 'Enquiry has been re assigned successfully';
                        $('#ShowMessgae').modal('show');
                    }
                });
            }

        }
        $scope.Reassigncounsellor = function (data) {
            console.log(data);
            $scope.isReassign = true;
            $scope.ReassignData = data;
            $scope.getComment(data);
            $scope.getApplicationRemark(data);
            $scope.selectCounsellorData = {};
        }



        $scope.getApplicationRemark = function (data) {
            var counseId = {};
            counseId.ApplicationId = data.enquiry._id;
            getCommentsByApplicationId(counseId).then(function (res) {
                // console.log(res.data);
                $scope.ClosingRemark = res.data;
            });

        }

        $scope.OpenModelFor = function (forData) {
            $scope.OpenModelForEnquiry = false;
            $scope.OpenModelForCounsellor = false;
            $scope.OpenModelForEnquiryOne = false;
            $scope.OpenModelForAbandon = false;

            if (forData == 'enquiry') {
                $scope.OpenModelForEnquiry = true;
                $('#exampleModalCenter').modal('show');

            }
            if (forData == 'enquiryOne') {
                $scope.LoadingWUFAD = true;
                $scope.OpenModelForEnquiryOne = true;
                $('#exampleModalCenter').modal('show');
            }
            if (forData == 'counsellor') {
                $scope.LoadingDC = true;
                $scope.OpenModelForCounsellor = true;
            }
            if (forData == 'abandon') {
                $scope.LoadingWUFAD = true;
                $scope.OpenModelForAbandon = true;
            }
            $('#exampleModalCenter').modal('show');

        }

        $scope.getEnquiryBycounsellorId = function (counId) {
            $scope.AllEnquiryOfCounsellor = [];
            $scope.counsellorId = counId;
            var counsellorData = [];
            $scope.pendingEnquiry = [];
            counsellorData = $scope.getAssignApplications;
            for (var data in counsellorData) {
                if (counsellorData[data].CounsellorId === $scope.counsellorId._id) {
                    $scope.AllEnquiryOfCounsellor.push(counsellorData[data]);
                    if (counsellorData[data].enquiry.ApplicationStatus == 'pending') {
                        $scope.pendingEnquiry.push(counsellorData[data]);
                    }
                }
            }
            if ($scope.AllEnquiryOfCounsellor) {
                $scope.CounsellorHaveEnquiry = true;
                $scope.isCounsellorView = false;
                $scope.isCounsellorViews = false;
            } else {
                $scope.NoEnquiryForCounsellor = true;
            }

        }
        $scope.RemoveCounsellor = function () {
            var CounsellorDeleteData = {};
            $scope.show = false;
            var RemarkData = {};

            $scope.commentsRemark.RemarkBy = "Admin";
            RemarkData.creatorId = AdminUserId;
            RemarkData.commentsRemark = $scope.commentsRemark;
            RemarkData.commentsCounsellorId = $scope.counsellorId._id;
            RemarkData.commentsCounsellorName = $scope.counsellorId.Name;
            RemarkData.commentsRole = $scope.counsellorId.Role;

            CounsellorDeleteData.CounsellorId = $scope.counsellorId._id;

            removeCounsellor(CounsellorDeleteData).then(function (res) {
                $scope.LoadingDC = false;
                if (res.data.message == "Counsellor deleted successfully") {
                    $scope.SuccessMessgage = 'Counsellor has been deleted successfully';
                    $scope.commentsRemark = {};
                    $scope.show = true;
                    GetCounsellor(getCounsellors);
                    $scope.backCounsellorHaveEnquiry();
                    $scope.backCounselorViews();
                    document.getElementsByName('addcouns').form.reset();
                    $scope.commentsRemark = {};
                    $scope.counsellorId = {};
                }
            });

            //call add comment Function
            addNewComment(RemarkData).then(function (res) {
                // console.log("res",res.data);
            });

        }


        $scope.ConvertedEnquires = function (data) {
            $scope.ConvertedEnquiresData = data;
            var counseId = {};
            counseId.creatorId = $scope.ConvertedEnquiresData.CounsellorId;
            //call get commnets
            getCommentsByApplicantId(counseId).then(function (res) {
                $scope.commentData = res.data;
            });
        }

        $scope.RemoveEnquiry = function (data, remrkData) {
            $scope.show = false;
            var RemoveData = {};
            var RemarkData = {};
            RemoveData.ApplicationId = data._id;
            $scope.commentsRemark.RemarkBy = "Admin";
            RemarkData.creatorId = AdminUserId;
            RemarkData.ApplicationId = data._id;
            RemarkData.commentsRemark = $scope.commentsRemark;
            if (remrkData != undefined || remrkData != null) {
                if (RemoveData.ApplicationId == undefined || RemarkData.creatorId == undefined) {
                    $scope.isRemoveData = true;
                } else {

                    //call add comment Function
                    addNewComment(RemarkData).then(function (res) {
                        // console.log("res",res.data);
                    });

                    //call remove enquiry
                    removeEnquiry(RemoveData).then(function (res) {

                        if (res.data.message == "Application deleted successfully") {
                            $scope.SuccessMessgage = 'Enquiry has been deleted';
                            $scope.show = true;
                            $scope.NewEnquiryData = {};
                            $scope.commentsRemark = {};
                            GetEnquiryListNotAssigned();
                        }
                    });
                }
            } else {
                $scope.pleaseSelectRemark = true;
            }

        }
        $scope.RemoveEnquiryOne = function(data, cmment){
            console.log("data", data, cmment);
            $scope.show = false;
            var RemoveData = {};
            var RemarkData = {};
            RemoveData.ApplicationId = data.enquiry._id;
            $scope.commentsRemark.RemarkBy = "Admin";
            RemarkData.creatorId = AdminUserId;
            RemarkData.ApplicationId = data.enquiry._id;
            RemarkData.commentsRemark = $scope.commentsRemark;
            console.log("Remark", RemarkData);
            if (cmment != undefined || cmment != null) {
                if (RemoveData.ApplicationId == undefined || RemarkData.creatorId == undefined) {
                    $scope.isRemoveData = true;
                } else {

                    //call add comment Function
                    addNewComment(RemarkData).then(function (res) {
                        console.log("res",res.data);
                    });
                    //call remove enquiry
                    removeEnquiry(RemoveData).then(function (res) {
                        $scope.LoadingWUFAD = false;
                        if (res.data.message == "Application deleted successfully") {
                            $scope.SuccessMessgage = 'Enquiry has been deleted';
                            $scope.show = true;
                            $scope.ReassignData= {};
                            $scope.commentsRemark = {};
                            GetAssignedEnquiry();
                        }
                    });
                }
            } else {
                $scope.pleaseSelectRemark = true;
            }
        }


        $scope.AddCounsellor = function () {
            $scope.isInvalidEmail = false;
            $scope.notSame = false;
            $scope.LoadingAC = true;
            $scope.dublicate = false;
            var CounsellorData = $scope.CounsellorData;
            if (CounsellorData == undefined || CounsellorData == null) {
                $scope.dataNull = true;
                $scope.LoadingAC = false;
            }if(! isEmailValid(CounsellorData.Email)){
                $scope.isInvalidEmail = true;
                $scope.LoadingAC = false;
            } else {
                    CounsellorData.Role = "counsellor";
                    CounsellorData.isActive = true;
                    CounsellorData.AssignAppictions = 0;
                    //add new counsellor
                    addNewCounsellor(CounsellorData).then(function (res) {
                        $scope.LoadingAC = false;
                        $scope.myResponse = res.data;
                        console.log("$scope.myResponse",res);
                        if ($scope.myResponse.errmsg) {
                            $scope.dublicate = true;
                        }
                        if ($scope.myResponse.message == "user has been created") {
                            // $scope.added = true;
                            $scope.SuccessMessgage = 'New counsellor Added Successfully.!';
                            $('#ShowMessgae').modal('show');
                            $scope.CounsellorData = {};
                            GetCounsellor(getCounsellors);
                            $scope.backAddCounsellor();
                            document.getElementsByName('addcouns').form.reset();
                        }else {
                            $scope.dublicate = true;
                            $scope.AlredyInUse = true;
                            $scope.LoadingAC = false;
                        }
                    });
            }
            function isEmailValid(email)
            {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
                {
                    return true;
                }
                return false;
            }
        }

        $scope.resetPassword = function () {
            if ($scope.resetPassData.newPassword !== $scope.resetPassData.againPassword) {
                $scope.NotMatch = true;
            } else {
                $scope.resetPassData.username = $scope.UserDetails.User.username;
                $scope.resetPassData.userId = $scope.UserDetails.User._id;
                $scope.LoadingCP = true;

                resetPassword($scope.resetPassData).then(function (res) {
                    $scope.LoadingCP = false;
                    if (res.data.message == "Password Changed successfully") {
                        $scope.SuccessMessgage = 'Your password has been changed successfully';
                        $('#ShowMessgae').modal('show');
                        $scope.resetPassData={};
                    }
                    if (res.data.message == "Invalid password") {
                        $scope.SuccessMessgage = 'Please Enter correct Old Password';
                        $('#ShowMessgae').modal('show');
                        $scope.resetPassData={};
                    }
                    if (res.data.message == "Please enter new password") {
                        $scope.SuccessMessgage = 'Password already in use, Please Enter new password';
                        $('#ShowMessgae').modal('show');
                        $scope.resetPassData={};
                    }
                });
            }
        }
        $scope.ChangeCounsellorPassword = function () {
            $scope.NotMatch = false;
            $scope.LoadingCRP=true;
            console.log("res",$scope.resetPassData, $scope.CounsellorViewData);
            if ($scope.resetPassData.newPassword !== $scope.resetPassData.againPassword) {
                $scope.NotMatch = true;
                $scope.LoadingCRP=false;
            } else {
                $scope.resetPassData.username = $scope.CounsellorViewData.username;
                $scope.resetPassData.userId = $scope.CounsellorViewData._id;

                resetPassword($scope.resetPassData).then(function (res) {
                    $scope.LoadingCRP=false;
                    if (res.data.message == "Password Changed successfully") {
                        $scope.SuccessMessgage = 'Password has been Changed successfully';
                        $('#ShowMessgae').modal('show');
                        $scope.resetPassData={};
                    }
                    if (res.data.message == "Please enter new password") {
                        $scope.SuccessMessgage = 'Password already in use, Please Enter new password';
                        $('#ShowMessgae').modal('show');
                        $scope.resetPassData={};
                    }
                });
            }
        }


        /* ...................................................................................comment Start............
               */
        $scope.getComment = function (data) {
            var counseId = {};
            counseId.ApplicationId = data.enquiry._id;
            getCommentsByApplicationId(counseId).then(function (res) {
                console.log(res.data);
                $scope.commentData = res.data;
            });
        }

        $scope.AddComments = function (data) {
            $scope.dataForSendComments = {};
            $scope.dataForSendComments.creatorId = $scope.UserDetails.User._id;
            $scope.dataForSendComments.ApplicationId = data;
            $scope.dataForSendComments.commentsBy = "admin";
            $scope.dataForSendComments.content = $scope.addCommentData.content;
            $scope.dataForSendComments.commentsRemark = $scope.addCommentData.commentsRemark;
            $scope.isCommentAdded= true;

            //call add comment Function
            addNewComment($scope.dataForSendComments).then(function (res) {
                $scope.commentResponse = res;
                $scope.addCommentData = {};
                $scope.commentData.push(res.data.data);
            });
        }

        $scope.Followupform = function (data) {
            $scope.FollowUp = true;
        }
        $scope.openForm = function () {
            document.getElementById("myForm").style.display = "block";
        }
        $scope.closeForm = function () {
            document.getElementById("myForm").style.display = "none";
        }
        /*...................................................................................comment end............
        */

        $scope.giveApproval = function(){
            $scope.ReassignData.enquiry.isAbandoned= false;
            var ConvertFormData = {};
            ConvertFormData.AssignDate = $scope.ReassignData.AssignDate;
            ConvertFormData.counsellorId = $scope.ReassignData.counsellorId;
            ConvertFormData.ApplicationNo = $scope.ReassignData.enquiry.ApplicationNo;
            ConvertFormData.ApplicationId = $scope.ReassignData.enquiry._id;
            ConvertFormData.ApplicationStatus = "abandoned";
            $scope.LoadingGRA = true;
            convertApplication(ConvertFormData).then(function (res) {
                $scope.LoadingGRA = false;
                if(res.data.success) {
                        var apprveData = {};
                        apprveData.isAbandoned = false;
                        apprveData.ApplicationId = ConvertFormData.ApplicationId;
                    makeIsAbandonFalse(apprveData).then(function (res) {
                            if(res.data.success){
                                $scope.SuccessMessgage = 'Enquiry has been Approved and Abandoned successfully';
                                $('#ShowMessgae').modal('show');
                            }
                        });
                    GetAssignedEnquiry();
                    $scope.ReassignData.enquiry.ApplicationStatus = "abandoned";
                }
                else {
                    $scope.SuccessMessgage = 'Something happen wrong! Please try again';
                    $('#ShowMessgae').modal('show');
                }
            });
        }
        $scope.rejectApproval = function () {
            var getData = {};
            var addComment = {};
            var reamarkData = {};

            getData.ApplicationId = $scope.ReassignData.enquiry._id;
            getData.isAbandoned = $scope.ReassignData.enquiry.isAbandoned;
            //comment data
            reamarkData.RemarkBy = "Admin";
            reamarkData.Remark = "Approval Rejected by Enquiry admin";
            addComment.commentsRemark = reamarkData;
            addComment.creatorId = AdminUserId;
            addComment.ApplicationId = $scope.ReassignData.enquiry._id;

            $scope.LoadingGRA = true;
            //call add comment Function
            addNewComment(addComment).then(function (res) {
                console.log("res",res.data);
            });

            //call isabandon make false
            makeIsAbandonFalse(getData).then(function (res) {
                console.log("res",res);
                $scope.ReassignData.enquiry.isAbandoned= false;
            });
            //call reject Approval function
            rejectApproval(getData).then(function (res) {
                $scope.LoadingGRA = false;
                if(res.data.message == "application rejected successfully"){
                    if($scope.calledByReassign== true){
                        $scope.SuccessMessgage = 'Request has been rejected and reassigned';
                        $('#ShowMessgae').modal('show');
                    }else {
                        $scope.SuccessMessgage = 'Request has been rejected';
                        $('#ShowMessgae').modal('show');
                    }
                }
            });
        }
        $scope.AbandonEnquiry = function () {
            $scope.show = false;
            if($scope.ReassignData.enquiry.isAbandoned == true){
                $scope.SuccessMessgage = 'Please reject approval and then try again';
                $('#ShowMessgae').modal('show');
                $scope.LoadingWUFAD = false;
            }else {
                if ($scope.commentsRemark.Remark != undefined || $scope.commentsRemark.Remark != null) {
                var RemarkData = {};
                $scope.commentsRemark.RemarkBy = "Admin";
                RemarkData.creatorId = AdminUserId;
                RemarkData.commentsRemark = $scope.commentsRemark;
                RemarkData.commentsApplicationId = $scope.ReassignData.enquiry._id;
                //call add comment Function
                addNewComment(RemarkData).then(function (res) {
                    console.log("res",res.data);
                });

                var AbandonData = {};
                AbandonData.ApplicationId = $scope.ReassignData.enquiry._id;
                AbandonData.ApplicationStatus = "abandoned";
                updateEnquiryStatus(AbandonData).then(function (res) {
                    $scope.LoadingWUFAD = false;
                    if (res.data.message == "Application Status updated successfully") {
                        $scope.SuccessMessgage = 'Enquiry has been abandon';
                        $scope.show = true;
                        $scope.ReassignData.enquiry.ApplicationStatus = "abandoned";
                    }
                });
                }else {
                    $scope.pleaseSelectRemark = true;
                }
            }
        }
        $scope.UpdateCounsellorInfo = function(){
            $scope.CounsellorViewData.UserId = $scope.CounsellorViewData._id;
            updateUserProfile($scope.CounsellorViewData).then(function (res) {
                 if(res.data.message== "user successfully updated"){
                     $scope.isEditDiv=false;
                     $scope.SuccessMessgage = 'Counsellor has been updated';
                     $('#ShowMessgae').modal('show');
                 }
            });
        }

         $scope.DeletedEnquiries = function () {
            $scope.isDeletedEnquiries = true;
            var deletedEnq = {};

            getAlldeletedEnquiry().then(function (res) {
               console.log("deleted enquiry", res.data);
                deletedEnq =res.data;
                // deletedEnq.enquiry =res.data;
                //  $scope.getAssignApplications = deletedEnq;
               for(var enq in deletedEnq){

               }

            });
        }





        /*sorting for work status*/
        $scope.checkNewAbandonRequest = function(){
            if($scope.newAbandonRequest.length > 0){
                $scope.getAssignApplications = $scope.newAbandonRequest;
                showPaginationData($scope.getAssignApplications);
            }else {

            }
        }

        $scope.refreshSort = function(){
            GetAssignedEnquiry();
            // $scope.isDeletedEnquiries = false;
            $scope.getAssignApplications =  $scope.getAssignApplicationsTemp;
            showPaginationData($scope.getAssignApplications);
        }

        $scope.SortEnquiry = function(){
            var SortData = $scope.sortInWorkStatusEnquiryData;
            var DataForSort = $scope.getAssignApplications;
            var DataForSortTemp = $scope.getAssignApplicationsTemp;
            var getType,CounsData,getCounsellor,getStatus,StatusData;
            console.log("SortData",SortData);
            console.log("$scope.getAssignApplications",$scope.getAssignApplications);

            if(SortData == undefined || SortData == null){
                $scope.getAssignApplications = DataForSortTemp;
            }
            if(SortData.Type){
                getType = sortWorkEnquiry(SortData.Type);
                $scope.getAssignApplications = getType;
            }
            if(SortData.Counsellor){
                CounsData = DataForSortTemp;
                getCounsellor = sortOrderWorkCounsellor(CounsData, SortData.Counsellor);
                $scope.getAssignApplications = getCounsellor;
            }
            if(SortData.Status){
                StatusData = DataForSortTemp;
                getStatus = sortOrderWorkStatus(StatusData, SortData.Status);
                $scope.getAssignApplications = getStatus;
                console.log("i amin status",getStatus);
            }
            if(SortData.Type && SortData.Counsellor){
                getType = sortWorkEnquiry(SortData.Type);
                getCounsellor = sortOrderWorkCounsellor(getType, SortData.Counsellor);
                $scope.getAssignApplications = getCounsellor;
            }
            if(SortData.Type && SortData.Status){
                getType = sortWorkEnquiry(SortData.Type);
                getStatus = sortOrderWorkStatus(getType, SortData.Status);
                $scope.getAssignApplications = getStatus;
            }if(SortData.Counsellor && SortData.Status){
                CounsData = DataForSortTemp;
                getCounsellor = sortOrderWorkCounsellor(CounsData, SortData.Counsellor);
                getStatus = sortOrderWorkStatus(getCounsellor, SortData.Status);
                $scope.getAssignApplications = getStatus;
            }
            if(SortData.Type && SortData.Counsellor && SortData.Status){
                getType = sortWorkEnquiry(SortData.Type);
                getCounsellor = sortOrderWorkCounsellor(getType, SortData.Counsellor);
                getStatus = sortOrderWorkStatus(getCounsellor, SortData.Status);
                $scope.getAssignApplications = getStatus;
            }
            showPaginationData($scope.getAssignApplications);

        }

        $scope.sortInWorkStatusEnquiry = function(sortFor){
            console.log("sortFor",sortFor);
            var tempArray = $scope.getAssignApplicationsTemp;
            if(sortFor == 'Status'){
                console.log(" $scope.sortInWorkStatusEnquiryData", $scope.sortInWorkStatusEnquiryData);
                console.log(" $scope.getAssignApplications", tempArray);
                sortOrderWorkStatus($scope.getAssignApplications ,$scope.sortInWorkStatusEnquiryData);
                $scope.sortStatus = $scope.sortInWorkStatusEnquiryData;
                console.log("$scope.sortStatus",$scope.sortStatus);

            }
            if(sortFor == 'EnquiryType'){
                console.log("$scope.sortInWorkStatusEnquiryData", $scope.sortInWorkStatusEnquiryData);
                console.log("$scope.getAssignApplications", tempArray);
                sortWorkEnquiry($scope.sortInWorkStatusEnquiryData);
            }
            if(sortFor == 'Counsellor'){
                sortOrderWorkCounsellor(tempArray ,$scope.sortInWorkStatusEnquiryData)
            }
        }



        function sortOrderEnquiryType (data) {
            var sortData = data;
            tempEnquiryList = data;
            Consulting_For1= [];
            Require_Cooks1 = [];
            Rent_Our_Kitchen1 = [];
            for (var sort1 in sortData) {
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Recipe Standardization' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Kitchen Setup' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Menu Planning' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Staff Training' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Consulting For : Operations' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Consulting For : Other'
                ) {
                    Consulting_For1.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : One Day Event'||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Restaurant' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Café' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Fine Dining' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Commercial/Delivery Kitchen' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Food truck' ||
                    sortData[sort1].enquiry.RequireCookChefFor == 'Require Cooks/Chefs For : Home' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==  'Require Cooks/Chefs For : Other') {
                    Require_Cooks1.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Food Trials'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Catering' ||
                    sortData[sort1].enquiry.RequireCookChefFor ==' Rent Our Kitchen For : Get to Gathers' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Photography'||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Events' ||
                    sortData[sort1].enquiry.RequireCookChefFor =='Rent Our Kitchen For : Other'
                ) {
                    Rent_Our_Kitchen1.push(sortData[sort1]);
                }
            }
        }
        function sortWorkEnquiry (data) {
            countOfSort = data;
            if (countOfSort == 1) {
                return Consulting_For1;
            }
            if (countOfSort == 2) {
                return Require_Cooks1;
            }
            if (countOfSort == 3) {
                return Rent_Our_Kitchen1;
            }
            if (countOfSort == 4) {
                return tempEnquiryList;
                countOfSort = 0;
            }
        }
        function sortOrderWorkCounsellor(data, CounsellorId) {
            var sortData = data;
            var ForCounsellor=[];
            for (var sort1 in sortData) {
                if (sortData[sort1].CounsellorId == CounsellorId) {
                    ForCounsellor.push(sortData[sort1]);
                }
            }
            return ForCounsellor;
        }
        function sortOrderWorkStatus(data, Status) {
            var sortData = data;
            var status = Status;
            var ForPending=[];
            var ForConverted=[];
            var ForAbandoned=[];
            for (var sort1 in sortData) {
                if (sortData[sort1].enquiry.ApplicationStatus =="pending") {
                    ForPending.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.ApplicationStatus =="converted") {
                    ForConverted.push(sortData[sort1]);
                }
                if (sortData[sort1].enquiry.ApplicationStatus =="abandoned") {
                    ForAbandoned.push(sortData[sort1]);
                }
            }
           return AddStatus(status,sortData, ForPending,ForConverted,ForAbandoned);
        }
        function AddStatus(Status,data, ForPending1,ForConverted1,ForAbandoned1) {
            var ForPending=ForPending1;
            var ForConverted=ForConverted1;
            var ForAbandoned=ForAbandoned1;

            if(Status == 'pending'){
               return ForPending;
            }if(Status == 'converted'){
                return ForConverted;
            }if(Status == 'abandoned'){
                return ForAbandoned;
            }if(Status == 'All'){
                 return data;
            }
        }
        function getAbandonRequest(data) {
            var abrData = [];
            for(var abr in data){
                if(data[abr].enquiry.isAbandoned == true){
                    abrData.push(data[abr]);
                }
            }
            return abrData;
        }

        /*sorting for work status*/

        /* pagination for work status/*/
         function showPaginationData(data){

            $scope.curPage = 0;
            $scope.pageSize = 10;
            $scope.datalists = [];
            $scope.datalists = data;
            $scope.numberOfPages = function() {
                return Math.ceil($scope.datalists.length / $scope.pageSize);
            };

         }









        $scope.profileCounsellor = function (counsellor) {
            $scope.CounsellorHaveEnquiry = false;
            $scope.isCounsellorView = false;
            $scope.isCounsellorViews = true;
            $scope.CounsellorViewData = counsellor;
            $('.navbar-collapse').collapse('hide');
        }
        $scope.backCounselorViews = function () {
            $scope.isCounsellorView = true;
            $scope.isCounsellorViews = false;
            $scope.CounsellorHaveEnquiry = false;
        }
        $scope.backCounselorView = function () {
            $scope.isAddCounsellors = true;
            $scope.ischangePassword = true;
            $scope.isCounsellors = false
        }
        $scope.counsellorAddUpdateDelete = function () {
            $scope.isCounsellors = true;
            $scope.isAddCounsellors = false;
            $scope.ischangePassword = true;
            $scope.isCounsellorView = true;
            $scope.isCounsellorViews = false;
            $scope.CounsellorHaveEnquiry = false;
        }
        $scope.backCounsellorHaveEnquiry = function () {
            $scope.CounsellorHaveEnquiry = false;
            $scope.isCounsellorView = false;
            $scope.isCounsellorViews = true;
        }
        $scope.backAddCounsellor = function () {
            $scope.isCounsellors = true;
            $scope.isAddCounsellors = false;
            $scope.ischangePassword = true;
            $scope.isCounsellorView = true;
        }

        $scope.doLogout = function () {
            Auth.logout();
            $location.path('/login');
        }
        $("#EnquiriesNav").addClass("navliactive");
        $scope.FuncWorkStatus = function () {
            $scope.searchText="";
            $("#EnquiryStatusNav").addClass("navliactive");
            $("#EnquiriesNav").removeClass("navliactive");
            $("#ProfileNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.AssignEnquiries = false;
            $scope.WorkStatus = true;
            $scope.EnquiryForms = false;
            $scope.Dashboard = false;
            $scope.isProfile = false;

        }
        $scope.FuncEnquiryForms = function () {
            $scope.AssignEnquiries = false;
            $scope.WorkStatus = false;
            $scope.EnquiryForms = true;
            $scope.Dashboard = false;
            $scope.isProfile = false;

        }
        $scope.FuncEnquires = function () {
            $scope.searchText="";
            $("#EnquiriesNav").addClass("navliactive");
            $("#EnquiryStatusNav").removeClass("navliactive");
            $("#ProfileNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.AssignEnquiries = true;
            $scope.WorkStatus = false;
            $scope.EnquiryForms = false;
            $scope.Dashboard = false;
            $scope.isProfile = false;
        }
        $scope.FuncProfile = function () {
            $("#ProfileNav").addClass("navliactive");
            $("#EnquiryStatusNav").removeClass("navliactive");
            $("#EnquiriesNav").removeClass("navliactive");
            $("#DashboardNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");
            $scope.AssignEnquiries = false;
            $scope.WorkStatus = false;
            $scope.EnquiryForms = false;
            $scope.Dashboard = false;
            $scope.isProfile = true;
        }
        $scope.FuncDashboard = function () {
            $("#DashboardNav").addClass("navliactive");
            $("#EnquiryStatusNav").removeClass("navliactive");
            $("#EnquiriesNav").removeClass("navliactive");
            $("#ProfileNav").removeClass("navliactive");
            $("body").removeClass("offcanvas");

            $scope.Dashboard = true;
            $scope.AssignEnquiries = false;
            $scope.WorkStatus = false;
            $scope.EnquiryForms = false;
            $scope.isProfile = false;


            $(function () {
                // $("#chartContainer").CanvasJSChart({ //Pass chart options
                //         data: [
                //             {
                //                 type: "splineArea", //change it to column, spline, line, pie, etc
                //                 dataPoints: [
                //                     {x: 10, y: 10},
                //                     {x: 20, y: 14},
                //                     {x: 30, y: 18},
                //                     {x: 40, y: 22},
                //                     {x: 50, y: 18},
                //                     {x: 60, y: 28}
                //                 ]
                //             }
                //         ]
                //     });


                $("#chartContainer1").CanvasJSChart({
                    title: {
                        text: "Enquiry in 2019"
                    },
                    subtitles: [{
                        text: "jan to may"
                    }],
                    animationEnabled: true,
                    data: [{
                        type: "pie",
                        startAngle: 40,
                        toolTipContent: "<b>{label}</b>: {y}%",
                        showInLegend: "true",
                        legendText: "{label}",
                        indexLabelFontSize: 16,
                        indexLabel: "{label} - {y}%",
                        dataPoints: [
                            {y: 48.36, label: "Require Cooks/Chefs For : Restaurant, Café"},
                            {y: 26.85, label: "Commercial/Delivery Kitchen"},
                            {y: 1.49, label: "Consulting For : Kitchen Setup"},
                            {y: 6.98, label: "Food truck"},
                            {y: 6.53, label: "One Day Event"},
                            {y: 2.45, label: "Recipe Standardization"},
                            {y: 3.32, label: "Staff Training"},
                            {y: 4.03, label: "Others"}
                        ]
                    }]
                });
            });

            /* var options = {
                 exportEnabled: true,
                 animationEnabled: true,
                 title:{
                     text: "Units Sold VS Profit"
                 },
                 subtitles: [{
                     text: "Click Legend to Hide or Unhide Data Series"
                 }],
                 axisX: {
                     title: "States"
                 },
                 axisY: {
                     title: "Units Sold",
                     titleFontColor: "#4F81BC",
                     lineColor: "#4F81BC",
                     labelFontColor: "#4F81BC",
                     tickColor: "#4F81BC",
                     includeZero: false
                 },
                 axisY2: {
                     title: "Profit in USD",
                     titleFontColor: "#C0504E",
                     lineColor: "#C0504E",
                     labelFontColor: "#C0504E",
                     tickColor: "#C0504E",
                     includeZero: false
                 },
                 toolTip: {
                     shared: true
                 },
                 legend: {
                     cursor: "pointer",
                     itemclick: toggleDataSeries
                 },
                 data: [{
                     type: "spline",
                     name: "Units Sold",
                     showInLegend: true,
                     xValueFormatString: "MMM YYYY",
                     yValueFormatString: "#,##0 Units",
                     dataPoints: [
                         { x: new Date(2016, 0, 1),  y: 120 },
                         { x: new Date(2016, 1, 1), y: 135 },
                         { x: new Date(2016, 2, 1), y: 144 },
                         { x: new Date(2016, 3, 1),  y: 103 },
                         { x: new Date(2016, 4, 1),  y: 93 },
                         { x: new Date(2016, 5, 1),  y: 129 },
                         { x: new Date(2016, 6, 1), y: 143 },
                         { x: new Date(2016, 7, 1), y: 156 },
                         { x: new Date(2016, 8, 1),  y: 122 },
                         { x: new Date(2016, 9, 1),  y: 106 },
                         { x: new Date(2016, 10, 1),  y: 137 },
                         { x: new Date(2016, 11, 1), y: 142 }
                     ]
                 },
                     {
                         type: "spline",
                         name: "Profit",
                         axisYType: "secondary",
                         showInLegend: true,
                         xValueFormatString: "MMM YYYY",
                         yValueFormatString: "$#,##0.#",
                         dataPoints: [
                             { x: new Date(2016, 0, 1),  y: 19034.5 },
                             { x: new Date(2016, 1, 1), y: 20015 },
                             { x: new Date(2016, 2, 1), y: 27342 },
                             { x: new Date(2016, 3, 1),  y: 20088 },
                             { x: new Date(2016, 4, 1),  y: 20234 },
                             { x: new Date(2016, 5, 1),  y: 29034 },
                             { x: new Date(2016, 6, 1), y: 30487 },
                             { x: new Date(2016, 7, 1), y: 32523 },
                             { x: new Date(2016, 8, 1),  y: 20234 },
                             { x: new Date(2016, 9, 1),  y: 27234 },
                             { x: new Date(2016, 10, 1),  y: 33548 },
                             { x: new Date(2016, 11, 1), y: 32534 }
                         ]
                     }]
             };
             $("#chartContainer2").CanvasJSChart(options);

             function toggleDataSeries(e) {
                 if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                     e.dataSeries.visible = false;
                 } else {
                     e.dataSeries.visible = true;
                 }
                 e.chart.render();
             }*/

            /* new Morris.Donut({
                 element: 'enquiryStatus',
                 data: [
                     {label: "Converted", value: $scope.CountList.converted},
                     {label: "pending", value: $scope.CountList.converted},
                     {label: "Abondon", value: $scope.CountList.converted}
                 ],
                 colors:['#D35C5C','#F9B54C', '#2D758C']
             }).on('click', function (i, row) {
                 // Do your actions
                 // Example:
                 displayFinanceChart(i, row);
             });*/

            /*  function displayFinanceChart(i, row) {
                  $('#dChart').html("Finance Chart of " + row.label + ": Total " + row.value);
                  $("#enquiryStatus").empty();
              }
  */


        }





        //All apis
        function reAssignCounsellor(data) {
            var getData = data;
            var updateAssignApplicationsToCounsellorUrl = url.urlBase + '/api/updateAssignApplicationsToCounsellor';
            return $http.post(updateAssignApplicationsToCounsellorUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function assignSingleApplication(data) {
            var getData = data;
            var assignApplicationsToCounsellorUrl = url.urlBase + '/api/assignApplicationsToCounsellor';
            return $http.post(assignApplicationsToCounsellorUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function assignMutliApplication(data) {
            var getData = data;
            var assignMultiApplicationsToCounsellorUrl = url.urlBase + '/api/assignMultiApplicationsToCounsellor';
            return $http.post(assignMultiApplicationsToCounsellorUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function resetPassword(data) {
            var getData = data;
            var setNewPasswordUrl = url.urlBase + '/api/setNewPassword';
            return $http.post(setNewPasswordUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function convertApplication(data) {
            var getData = data;
            var convertApplicationUrl = url.urlBase+'/api/convertApplication';
            return $http.post(convertApplicationUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function updateEnquiryStatus(data) {
            var getData = data;
            var updateEnquiryStatusUrl = url.urlBase + '/api/updateEnquiryStatus';
            return $http.post(updateEnquiryStatusUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function updateUserProfile(data) {
            var getData = data;
            var updateUserProfileUrl = url.urlBase+'/api/updateUserProfile';
            return $http.post(updateUserProfileUrl,getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function changePassword(data) {
            var getData = data;
            var resetPasswordUrl = url.urlBase + '/api/resetPassword';
            return $http.post(resetPasswordUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            })
        }
        function addNewCounsellor(data) {
            var getData = data;
            var addNewCounsellorUrl = url.urlBase + '/api/addNewCounsellor';
            return  $http.post(addNewCounsellorUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function removeEnquiry(data) {
            var getData = data;
            var removeEnquiryUrl = url.urlBase + '/api/removeEnquiry';
            return $http.post(removeEnquiryUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function removeCounsellor(data) {
            var getData = data;
            var removeCounsellorUrl = url.urlBase + '/api/removeCounsellor';
            return $http.post(removeCounsellorUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function getCommentsByApplicantId(data) {
            var getData = data;
            var getCommentsByApplictionIdUrl = url.urlBase + '/api/getCommentsByApplicationId';
            return $http.post(getCommentsByApplictionIdUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function getCommentsByApplicationId(data) {
            var getData = data;
            var getCommentsByCommentsApplicationIdUrl = url.urlBase + '/api/getCommentsByCommentsApplicationId';
            return $http.post(getCommentsByCommentsApplicationIdUrl, getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function addNewComment(data) {
            var CommentData = data;
            var addCommentsUrl = url.urlBase + '/api/addComments';
            return $http.post(addCommentsUrl, CommentData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function makeIsAbandonFalse(data) {
            var canelData = data;
            var cancelAbandonedUrl = url.urlBase +'/api/cancelAbandoned';
            return $http.post(cancelAbandonedUrl,canelData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function rejectApproval(data) {
            var approvaldata = data;
            var rejectApprovalUrl = url.urlBase +'/api/rejectApproval';
            return $http.post(rejectApprovalUrl,approvaldata, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }
        function makeApprovalFalse(data) {
            var getData = data;
            var falseApprovalUrl = url.urlBase +'/api/falseApproval';
            return $http.post(falseApprovalUrl,getData, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });
        }
        function getAlldeletedEnquiry() {
            var getDeletedAllEnquiryUrl = url.urlBase + '/api/getDeletedAllEnquiry';
            return $http.get(getDeletedAllEnquiryUrl,{
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }
            });

        }












        $(document).ready(function() {
            $('.bxslider_content').bxSlider({
                mode: 'fade',
                auto: true,
                autoControl: true,
                speed: 10
            });

            $('ul.nav li.dropdown-submenu').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(50);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(50);
            });
        });
    })
    .filter('pagination', function() {
    return function(input, start)
    {
        if(input == undefined){
            console.log("input null");
        }
        start = +start;
        return input.slice(start);
    };
});
