/**
 * Created by viraj on 12/04/2019.
 */
angular.module('enquiryCtrl', [])
.controller('enquiryController',function($scope, $http , $window, $location, url) {
 console.log("i am in enquiry controller");

    var econ = this;
    econ.ClientData = {};
    econ.EmailNotValid = false;
    
    econ.AddUserEnquiryData = function () {



        if(isEmpty(econ.ClientData.Name)){
            econ.NameNotEmpty =true;
            alert("You have entered an empty name!");

        }else if(isEmpty(econ.ClientData.Contact)){
            econ.ContactNotEmpty = true;
            alert("You have entered an empty contact!");
        }else if(isEmpty(econ.ClientData.Email)){
            econ.EmailNotEmpty = true;
            alert("You have entered an empty email!");
        }else if(! isEmailValid(econ.ClientData.Email)){
            alert("You have entered an invalid email address!");
        }else {
            //add browser info client ip url and save in db
            var userAgent = $window.navigator.userAgent;
            econ.ClientData.ApplicationStatus ="pending";
            econ.ClientData.ApplicationNo ="HireCooks_EID_";
            econ.ClientData.userAgent =userAgent;

            var addUserEnquiryUrl = url.urlBase+'/api/addUserEnquiry';
            $http.post(addUserEnquiryUrl,econ.ClientData).then(function (res) {
                $('#myModal').modal('hide');
                if(res.data.message == "enquiry has been created"){
                    $location.path('thankyou');
                }
            });
        }



}

    function isEmailValid(email)
    {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
        {
            return true;
        }
        return false;
    }
    function isEmpty(data) {
        if(data == undefined || data == null){
            return true;
        }
        return false;
    }


    $(document).ready(function() {
        $(":animated");
        $('.bxslider_content').bxSlider({
            mode: 'fade',
            auto: true,
            autoControl: true,
            speed: 10
        });

        $('ul.nav li.dropdown-submenu').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(50);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(50);
        });
    });

});