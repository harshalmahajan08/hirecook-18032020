/**
 * Created by viraj on 3/13/2019.
 */
angular.module('enquiryService',[])
.factory('enquiry', function($http, $window) {

	var enquiryFactory = {};

	enquiryFactory.createEnquiry = function(enquiryData){
		return $http.post('/api/addUserEnquiry', enquiryData);
	}
	enquiryFactory.saveEnquiry = function(enquiryData){
		return $http.post('/api/saveEnquiry', enquiryData, { headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': $window.localStorage['token']
                }});
	}
    enquiryFactory.getBranches = function(data){
        return $http.post('/api/schoolBranchApi/getBranchesBySchoolId', data, {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': $window.localStorage['token']
            }});
    }
    enquiryFactory.getUser = function(myEnquiryData){
        return $http.post("/api/users",myEnquiryData,{
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': $window.localStorage['token']
            }
        });
    }

	return enquiryFactory;
});
